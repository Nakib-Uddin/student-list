<x-app>
  
        <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <form action="{{ route('supdate',$student->id)}}" method="post"enctype="multipart/form-data">
        {{ csrf_field() }}
            <label>Name:</label>
            <input type="text" name="name" value="{{ $student->name }}"><br>
            <label for="birthday">Birthday:</label>
          <input type="date" id="birthday" name="dof"><br>
            <label>Gender</label>
            <input type="radio" name="gender" value="male">male
            <input type="radio" name="gender" value="female">female <br>
            
            <label>Hobbies</label>
            <input type="checkbox" name="food[]" value="pet">pet
            <input type="checkbox" name="food[]" value="garden">garden
            <input type="checkbox" name="food[]" value="food">food
             <br>
             <select name="nationality" id="">
                <option value="Bangladesh">Bangladesh</option>
                <option value="pakistan">pakistan</option>
             </select>
             <br>
            <input type="submit" value="save" name="submit">
        </form>
    </body>
    </html>
    
</x-app>