<?php

namespace App\Http\Controllers;

use App\Models\student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function slist()
    {
        $student = student::all();
        return view('student-list', ['student' => $student]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $foods = $request['food'];
        //$nationality = $request['nationality'];
        $request['food'] = implode(',', $foods);
        //$request['nationality'] = implode(',', $nationality);
        $data = ['name' => $request['name'], 'dof' => $request['dof'], 'gender' => $request['gender'], 'food' => $request['food'], 'nationality' => $request['nationality']];

        student::create($data);

        return redirect()->route('slist');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\student  $student
     * @return \Illuminate\Http\Response
     */
    public function sedit(student $student, $id)
    {
        $student = student::find($id);
        return view('sedit')->with('student', $student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\student  $student
     * @return \Illuminate\Http\Response
     */
    public function supdate(Request $request, student $student, $id)
    {
        $student = student::find($id);
        $foods = $request['food'];
        //$nationality = $request['nationality'];
        $request['food'] = implode(',', $foods);
        // $request['nationality'] = implode(',', $nationality);
        $data = ['name' => $request['name'], 'dof' => $request['dof'], 'gender' => $request['gender'], 'food' => $request['food'], 'nationality' => $request['nationality']];

        $student->update($data);

        return redirect()->route('slist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\student  $student
     * @return \Illuminate\Http\Response
     */
    public function sdelete(student $student, $id)
    {
        $student = student::find($id);
        $student->delete();
        return redirect()->route('slist');
    }
}
