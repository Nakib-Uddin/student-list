<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/create', [CategoryController::class, 'create'])->name('create');
Route::post('/store', [CategoryController::class, 'store'])->name('store');
Route::get('/list', [CategoryController::class, 'list'])->name('list');
Route::get('/delete/{id}', [CategoryController::class, 'delete'])->name('delete');
Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('edit');
Route::post('/update{id}', [CategoryController::class, 'update'])->name('update');
////
Route::get('/screate', [StudentController::class, 'create'])->name('screate');
Route::post('/store', [StudentController::class, 'store'])->name('sstore');
Route::get('/list', [StudentController::class, 'slist'])->name('slist');
Route::get('/delete/{id}', [StudentController::class, 'sdelete'])->name('sdelete');
Route::get('/edit/{id}', [StudentController::class, 'sedit'])->name('sedit');
Route::post('/update{id}', [StudentController::class, 'supdate'])->name('supdate');
